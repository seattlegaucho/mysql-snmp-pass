Name:           mysql-agent
Version:        1.0rc2_pp
Release:        13%{?dist}
Summary:        SNMP monitoring agent for MySQL

Group:          Applications/Databases
License:        GPL
URL:            http://www.masterzen.fr/software-contributions/mysql-snmp-monitor-mysql-with-snmp
# Source0:        %{name}-%{version}.tar.gz
# BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Source0:        MySQL-SNMP-v1.0_2.tar.gz
BuildRoot:      %{_tmppath}/MySQL-SNMP-%{version}-%{release}-root-%(%{__id_u} -n)
BuildRequires:	perl(Module::Build)

BuildArch:      noarch
Requires:       perl(DBI), perl(DBD::mysql) >= 1.0, perl(Unix::Syslog)
Requires:       perl(SNMP), perl(NetSNMP::OID), perl(NetSNMP::agent), perl(NetSNMP::ASN)
Requires:       perl(NetSNMP::agent::default_store), perl(NetSNMP::default_store)
Requires:       net-snmp
#Requires:       net-snmp >= 5.4.2

%description
mysql-agent is a small daemon that connects to a local snmpd daemon
to report statistics on a local or remote MySQL server.

mysql-ppsnmp is a script that uses the pass_persist mode of operation for
older releases which lack functional agentx support.

%prep
%setup -q -n MySQL-SNMP-v1.0_2
test "$RPM_BUILD_ROOT" == "/" || rm -rf "$RPM_BUILD_ROOT"
perl Build.PL --installdirs vendor --destdir "$RPM_BUILD_ROOT" --create_packlist 0

%install
perl Build install
install -d ${RPM_BUILD_ROOT}%{_sbindir}
install -d ${RPM_BUILD_ROOT}%{_initrddir}
install -d ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig
install -d ${RPM_BUILD_ROOT}%{_sysconfdir}/snmp
install -d ${RPM_BUILD_ROOT}%{_mandir}/man1
install -d ${RPM_BUILD_ROOT}%{_datadir}/snmp/mibs
install -c -m 755 mysql-agent ${RPM_BUILD_ROOT}%{_sbindir} 
install -c -m 755 redhat/mysql-agent.init ${RPM_BUILD_ROOT}%{_initrddir}/%{name}
install -c -m 644 redhat/mysql-agent.sysconfig ${RPM_BUILD_ROOT}%{_sysconfdir}/sysconfig/%{name}
install -c -m 600 my.cnf ${RPM_BUILD_ROOT}%{_sysconfdir}/snmp
install -c -m 644 mysql-agent.1 ${RPM_BUILD_ROOT}%{_mandir}/man1
gzip ${RPM_BUILD_ROOT}%{_mandir}/man1/mysql-agent.1 
install -m 644 MYSQL-SERVER-MIB.txt ${RPM_BUILD_ROOT}%{_datadir}/snmp/mibs

%clean


%files
%defattr(-,root,root,-)
%doc COPYING README opennms/*
%{_sbindir}/*
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}
%config(noreplace) %{_sysconfdir}/snmp/my.cnf
%doc %{_mandir}/man1/*.1*
%doc %{_mandir}/man3/*.3pm*
%{_initrddir}/*
%{_datadir}/snmp/*
%{_datadir}/snmp/mibs/*
%{perl_vendorlib}/MySQL/*

%changelog
* Tue Jul 12 2011 Marc Martinez <Marc.Martinez@openmarket.com> - 1.0rc2_pp-13
Expose upper and lower variants of the "bytes-behind-master" results to
allow the value to make it through unpatched older net-snmp releases
(albeit with some assembly required on the far end)
* Mon Jun 20 2011 Marc Martinez <Marc.Martinez@openmarket.com> - 1.0rc2_pp-12
Catch undefined variables within the slave handling code for running on a
standalone instance, and ensure that any perl interpreter warnings do not
leak to STDERR and cause the snmpd process to reap the ppsnmp script
* Wed May 11 2011 Marc Martinez <Marc.Martinez@openmarket.com> - 1.0rc2_pp-11
Silence warnings/errors for failure to connect to master when calculating
the "bytes behind master" drift
* Mon May  9 2011 Marc Martinez <Marc.Martinez@openmarket.com> - 1.0rc2_pp-10
Fix for de-referencing the "slave_sql_running" test to avoid compile errors
* Fri May  6 2011 Marc Martinez <Marc.Martinez@openmarket.com> - 1.0rc2_pp-9
Expose the mySlaveLagBytes as a Counter64, map it to Gauge64 within opennms
* Fri May  6 2011 Marc Martinez <Marc.Martinez@openmarket.com> - 1.0rc2_pp-8
Update the MySQL::SNMP module to add "bytes behind master" tracking as an
extra option flag to be coupled with the slave status
* Tue Feb 22 2011 Marc Martinez <Marc.Martinez@amdocs.com> - 1.0rc2_pp-6
Bundle up the pass_persist branch with core code copied to modules (still
need to update the original mysql-agent binary with a later release)
* Sat Oct 31 2009 Brice Figureau <brice@daysofwonder.com> - 1.0rc1-1
New version
* Sat Oct 24 2009 Brice Figureau <brice@daysofwonder.com> - 0.8-1
New version
Manpage compression in the spec
* Mon Sep 28 2009 Robin Bowes <rpmbuild@yo61.net> - 0.7-2
Add opennms config files to package
* Mon Sep 28 2009 Robin Bowes <rpmbuild@yo61.net> - 0.7-1
Initial RPM packaging
